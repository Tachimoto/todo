// タスクテーブル
var taskTable = {

    props: {
        taskList: Array,
    },

    template: `
        <table cellpadding="5px">
            <tr align="left">
                <th>No</th>
                <th width="150px">タスク名</th>
                <th width="200px">開始日</th>
                <th width="200px">期限</th>
                <th width="100px">場所</th>
                <th>優先度</th>
            </tr>
            <template v-for="(item, index) in taskList">
                <tr v-on:click="$emit('on-row-click', item, index)">
                    <td>{{ index + 1 }}</td>
                    <td>{{ item.name }}</td>
                    <td>{{ item.start }}</td>
                    <td>{{ item.end }}</td>
                    <td>{{ item.place }}</td>
                    <td>{{ item.priority }}</td>
                </tr>
            </template>
        </table>
    `,
};

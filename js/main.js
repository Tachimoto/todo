var vm = new Vue({
    el: '#todo-app',

    // 使用するコンポーネントを指定
    components: {
        'task-table': taskTable,
    },

    // データ
    data: {
        // message: 'Hello',

        // タスクリスト
        taskList: [],

        // フォームの入力値（画面表示時の初期値を代入している）
        record: {
            index: null,
            name: '打ち合わせ',
            start: '2020-06-01T09:00',
            end: '2020-06-01T12:00',
            place: '第一会議室',
            priority: '高',
        },

        // 場所リスト
        placeList: [
            '',
            '第一会議室',
            '第二会議室',
            '第三会議室',
        ],

        // 優先度リスト
        priorityList: [{
            id: 'low',
            value: '低',
        }, {
            id: 'mid',
            value: '中',
        }, {
            id: 'hig',
            value: '高',
        }],
    },

    /*
    // 算出プロパティ
    computed: {
        reversedMessage: {
            get: function () {
            // `this` は vm インスタンスを指します
                return this.message.split('').reverse().join('')
            },
            set: function(newValue) {
                this.message = newValue.split('').reverse().join('')
            },
        },
    },
    */

    // 関数
    methods: {
        // 登録ボタンのクリックイベント
        onClick: function() {
            const me = this,
                record = me.record,
                index = record.index;

            // indexが設定されてない場合は配列に追加する
            if (index === null) {
                this.taskList.push(record);
            } else {
                // 設定されている場合は更新する
                this.taskList.splice(index, 1, record);
            }

            // フォームの値をクリア
            this.clearRecord();
        },

        // フォームの値をクリア
        clearRecord: function() {
            this.record = {
                index: null,
                name: '',
                start: '',
                end: '',
                place: '',
                priority: '中',
            };
        },

        // 行クリックイベント
        onRowClick: function(item, index) {
            // indexに値を追加し、フォームの値を上書き
            this.record = {
                index: index,
                name: item.name,
                start: item.start,
                end: item.end,
                place: item.place,
                priority: item.priority,
            };
        },
    },
})
